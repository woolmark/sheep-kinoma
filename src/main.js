//@program
var fps = 24;
var sheepMax = 100;
var displayWitdh = 320;
var displayHeight = 240;

var fence = new Texture("background.png");
var sheep00 = new Texture("sheep00.png");
var sheep01 = new Texture("sheep01.png");

var skySkin = new Skin( { fill:"#9696FF" } );
var fenceSkin = new Skin( { width:fence.width, height:fence.height, texture:fence } );
var grandSkin = new Skin( { fill:"#63FF63" } );
var sheep00Skin = new Skin( { width:sheep00.width, height:sheep00.height, texture:sheep00 } );
var sheep01Skin = new Skin( { width:sheep01.width, height:sheep01.height, texture:sheep01 } );
var labelStyle = new Style( { font: "bold 18px", color:"black" } );

var init = false;
var runnable;
var sheep_image;
var sheep_pos;
var add_sheep;
var action;
var sheep_number;


var woolmarkBehavior = {
	onDisplayed: function(container) {
		init = true;
    	runnable = true;
    	sheep_pos = [];
    
	    for(var i = 0; i < sheepMax; i++) {
    	    sheep_pos[i] = [];
        	sheep_pos[i][0] = 0;
        	sheep_pos[i][1] = -1;
        	sheep_pos[i][2] = 0;
        	sheep_pos[i][3] = 0;
    	}

   		sheep_pos[0][0] = displayWitdh + 10;
    	sheep_pos[0][1] = displayHeight - 40;
    	sheep_pos[0][3] = getJumpX(sheep_pos[0][1]);
    	
    	sheep_image = [];
    	sheep_image[0] = sheep00Skin;
    	sheep_image[1] = sheep01Skin;
 
    	add_sheep;
    	action = 0;
   		sheep_number = 0;
        
        container.interval = 1000/fps;
       	container.start();
	},
	onTouchBegan: function(container, id, x, y, ticks) {
		add_sheep = true;
	},
	onTouchEnded: function(container, id, x, y, ticks) {
		add_sheep = false;
	},
	onTimeChanged: function(container) {
		if(init) {
			calc();
			draw(container);
		}
    }
}

function addSheep() {
    for(var i = 1; i < this.sheep_pos.length; i++) {
        if(this.sheep_pos[i][1] === -1) {
            this.sheep_pos[i][0] = (displayWitdh - displayWitdh % 10) + 10;
            this.sheep_pos[i][1] = displayHeight - (Math.floor(Math.random() * fence.height * 1.7));
            this.sheep_pos[i][2] = 0;
            this.sheep_pos[i][3] = getJumpX(sheep_pos[i][1]);
            return;
        }
    }
}

function getJumpX(y) {
    y -= (displayHeight - 40);
    return (70 - 3 * y / 4);
}

function draw(container) {
    container.empty(2, container.length);
    for(l = 0; l < sheep_pos.length; l++) {
        if(sheep_pos[l][1] > 0) {
            container.add(new Content({left:sheep_pos[l][0], right:0, top:sheep_pos[l][1], bottom:0, skin:sheep_image[action]}));
        }
    }
    container.add(new Label({left:-displayHeight, right:0, height: 18, top: 24, string: "Sheep " + sheep_number, style:labelStyle}));
}

function calc() {
	if(add_sheep) {
        addSheep();
    }

    for(var i = 0; i < sheep_pos.length; i++) {
        if(sheep_pos[i][1] >= 0) {
            sheep_pos[i][0] -= 5;

            if((sheep_pos[i][3] - 20) < sheep_pos[i][0] && sheep_pos[i][0] <= sheep_pos[i][3]) {
                sheep_pos[i][1] -= 3;
            }

            else {
                if((sheep_pos[i][3] - 40) < sheep_pos[i][0] && sheep_pos[i][0] <= (sheep_pos[i][3] - 20)) {
                    sheep_pos[i][1] += 3;
                }
                if(sheep_pos[i][0] < (sheep_pos[i][3] - 10) && sheep_pos[i][2] === 0) {
                    sheep_number++;
                    sheep_pos[i][2] = 1;
                }
            }

            // remove a frameouted sheep
            if(sheep_pos[i][0] < (-20 - displayWitdh)) {
                if(i === 0) {
                    sheep_pos[0][0] =
                        (displayWitdh - displayWitdh % 10) + 10;
                    sheep_pos[0][2] = 0;
                    sheep_pos[0][3] = getJumpX(sheep_pos[0][1]);
                } else {
                    sheep_pos[i][0] = 0;
                    sheep_pos[i][1] = -1;
                    sheep_pos[i][2] = 0;
                    sheep_pos[i][3] = 0;
                }
            }

        }

    }

    action = 1 - action;
}

var mainCon = new Container({
	left:0, right:0, top:0, bottom:0,
	skin: skySkin,
	contents:[
		new Content({left:0, right:0, top:displayHeight - fence.height, bottom:0, skin:grandSkin}),
		new Content({left:displayWitdh - fence.width - displayWitdh/2, right:0, top:displayHeight - fence.height, bottom:0, skin:fenceSkin})
	],
	behavior: woolmarkBehavior,
	active: true
});

application.add(mainCon);